# sysfs conda recipe

Home: "https://github.com/paulscherrerinstitute/sysfs"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS sysfs module
